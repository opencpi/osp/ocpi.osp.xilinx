# FSK_MODEM Implementation
---------------------------------------------------------------------------------

The fsk_modem assembly will **NOT** be built by default, this is done so that the 
user can bypass an error seen within the framework and still execute the testbias 
application in the zcu111 Getting Started Guide.   

If the user would like to implement, build and run the fsk_modem assembly please  
review the zcu111 Development Guide Guide.md located in the following directory:  
/home/user/opencpi/projects/osps/ocpi.osp.xilinx/guide/zcu111/Guide.md   
Specifically the section labeled **Building the fsk_modem HDL Assemblies/Containers**.  
