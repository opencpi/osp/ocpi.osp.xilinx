[//]: # (These are reference links used in the body of this note and get 
         stripped out when the markdown processor does its job. The blank lines
         before and after these references are important for portability across
         different markdown parsers.)


[xilinx-docs]:  <https://opencpi.gitlab.io/releases/latest/rst/osp_xilinx>

# xilinx

## Provided 
---
The following is provided within this repo:  
- MicroZed OSP zcu102 
- OSP Development Guide zcu102
- Code blocks for each design stage of the Development Guide as a reference   

## Getting started
---
See hdl/platforms/zcu102/zcu102-gsg.rst (source) and [opencpi.gitlab.io][xilinx-docs] (HTML)

## Guide for developing an OpenCPI Board Support Package (OSP) - Case Study zcu102
---
See Guide.md located in the guide/zcu102 directory  

## Version tested against
---
These guides and the associated OSP were tested against and are compatible with the following OpenCPI version:  
v2.4.2 

Notes:
---
  - hdl/assemblies - contains copies of assemblies from the OpenCPI assets project,
      with the addition of ZCU102-specific container XML files. A desirable framework
      improvement would be to allow the use of containers
      defined within the OSP in the appropriate assemblies without having to
      copy the assemblies into the OSP.


